﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mastermind
{
    class Program
    {
        static void Main(string[] args)
        {
            string codeSecret = "";
            string tentative = "";
            int tour = 1;
            List<char> historique = new List<char>();

            #region Accueil

            #region Affichage écran de démarrage
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"*           ___  ___          _                      _           _            *");
            Console.WriteLine(@"*           |  \/  |         | |                    (_)         | |           *");
            Console.WriteLine(@"*           | .  . | __ _ ___| |_ ___ _ __ _ __ ___  _ _ __   __| |           *");
            Console.WriteLine(@"*           | |\/| |/ _` / __| __/ _ \ '__| '_ ` _ \| | '_ \ / _` |           *");
            Console.WriteLine(@"*           | |  | | (_| \__ \ ||  __/ |  | | | | | | | | | | (_| |           *");
            Console.WriteLine(@"*           \_|  |_/\__,_|___/\__\___|_|  |_| |_| |_|_|_| |_|\__,_|           *");
            Console.WriteLine(@"*                                                                             *");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"* Règles :                                                                    *");
            Console.WriteLine(@"* - Trouver la bonne combinaison de 4 couleurs en 12 coups maximum.           *");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"* Légende :                                                                   *");
          //Console.WriteLine(@"* 1) Le nombre de + indique le nombre de bonnes couleurs à la mauvaise place. *");
            Console.WriteLine(@"* 1) Le nombre de * indique une bonne couleur à la bonne place.               *");
            Console.WriteLine(@"* 2) Le nombre de - indique une mauvaise couleur au mauvais endroit.          *");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"* Modes de jeu :                                                              *");
            Console.WriteLine(@"* 1) Un joueur        ====>     Appuyez sur 1 puis Enter                      *");
            Console.WriteLine(@"* 1) Deux joueurs     ====>     Appuyez sur 2 puis Enter                      *");
            Console.WriteLine(@"*******************************************************************************");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            #endregion

            #region Choix du mode de jeu
            int nbJoueurs;
            Console.WriteLine("Choisissez un mode de jeu (1/2) : ");
            do
            {
                while ((!int.TryParse(Console.ReadLine(), out nbJoueurs)))
                {
                    Console.WriteLine("Choisissez un mode de jeu (1/2) : ");
                }
            } while ((nbJoueurs < 1) || (nbJoueurs > 2));
            Console.Clear();
            #endregion

            #endregion

            #region Partie

            if (nbJoueurs == 1)
            {
                #region Génération du code secret
                Random aleatoire = new Random();
                int nbGenere;


                for (int i = 0; i < 4; i++)
                {
                    nbGenere = aleatoire.Next(1, 5);
                    codeSecret += nbGenere;
                }
                //Console.WriteLine(codeSecret);
                //Console.ReadKey();
                #endregion
                do
                {
                    Console.Clear();
                    tentative = "";

                    #region Affichage de l'historique
                    if(historique.Count() != 0)
                        Console.Write("******************** HISTORIQUE ********************");
                    for (int i = 0; i < (historique.Count()) / 8; i++)
                    {
                        Console.WriteLine("\n");
                        for (int j = i * 8; j < ((i * 8) + 8); j++)
                        {
                            Console.ForegroundColor = ConsoleColor.Black;
                            switch (historique.ElementAt(j))
                            {
                                case '1':
                                    Console.BackgroundColor = ConsoleColor.Blue;
                                    Console.Write("1");
                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.Write("\t");
                                    break;
                                case '2':
                                    Console.BackgroundColor = ConsoleColor.Green;
                                    Console.Write("2");
                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.Write("\t");
                                    break;
                                case '3':
                                    Console.BackgroundColor = ConsoleColor.Red;
                                    Console.Write("3");
                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.Write("\t");
                                    break;
                                case '4':
                                    Console.BackgroundColor = ConsoleColor.Yellow;
                                    Console.Write("4");
                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.Write("\t");
                                    break;
                                case '-':
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write(" -  ");
                                    break;
                                case '*':
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write(" *  ");
                                    break;
                            }
                        }
                    }

                    Console.WriteLine("\n\n****************************************************");
                    #endregion

                    #region Affichage de la légende
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.Write("\nLégende : 1 = ");
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("■\t");
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.Write("2 = ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("■\t");
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.Write("3 = ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("■\t");
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.Write("4 = ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("■");
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.WriteLine("\n\n****************************************************");
                    #endregion

                    #region Tentative
                    if (tour == 12)
                        Console.WriteLine("Attention, c'est votre dernière chance !");
                    else
                        Console.WriteLine($"Vous êtes au tour {tour}, il vous reste {13 - tour} tentatives.");
                    do
                    {
                        Console.WriteLine("Entrez 4 chiffres de 1 à 4 (voir légende) : ");
                        tentative = Console.ReadLine();
                    } while ((tentative.Length != 4) ||
                             ((tentative[0] < '1') || (tentative[0] > '4')) ||
                             ((tentative[1] < '1') || (tentative[1] > '4')) ||
                             ((tentative[2] < '1') || (tentative[2] > '4')) ||
                             ((tentative[3] < '1') || (tentative[3] > '4'))
                             );
                    Console.WriteLine(tentative);
                    #endregion

                    #region Mise à jour de l'historique


                    foreach (char c in tentative)
                    {
                        historique.Add(c);
                    }
                    for (int i = 0; i < 4; i++)
                    {
                        historique.Add(tentative[i] == codeSecret[i] ? '*' : '-');
                    }
                    Console.WriteLine("\n");
                    #endregion

                    tour++;
                } while ((tentative != codeSecret) && (tour < 13));
            }
            else
            {
                #region Choix du code secret
                int nbTotal, nb1, nb2, nb3, nb4;

                do
                {
                    Console.WriteLine("Entrez le code secret : ");
                    codeSecret = Console.ReadLine();

                    nbTotal = int.Parse(codeSecret);
                    nb1 = nbTotal / 1000;
                    nb2 = (nbTotal - (nb1 * 1000)) / 100;
                    nb3 = (nbTotal - ((nb1 * 1000) + (nb2 * 100))) / 10;
                    nb4 = nbTotal - ((nb1 * 1000) + (nb2 * 100) + (nb3 * 10));
                } while (
                         ((nb1 < 1) || (nb1 > 4)) ||
                         ((nb2 < 1) || (nb2 > 4)) ||
                         ((nb3 < 1) || (nb3 > 4)) ||
                         ((nb4 < 1) || (nb4 > 4))
                        );

                Console.WriteLine(codeSecret);
                Console.ReadKey();
                #endregion
                do
                {
                    #region Affichage de l'historique

                    #endregion

                    #region Affichage de la légende
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.Write("Légende : 1 = ");
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("■\t");
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.Write("2 = ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("■\t");
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.Write("3 = ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("■\t");
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.Write("4 = ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("■\t");
                    Console.ForegroundColor = ConsoleColor.White;
                    #endregion

                    #region Tentative

                    #endregion
                    tour++;
                } while ((tentative != codeSecret) && (tour < 12));
            }  

            #endregion

            #region Fin de partie
            if (tour < 13)
            {
                #region Gagné
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Green;
                Console.ForegroundColor = ConsoleColor.DarkGreen;

                Console.WriteLine(@"          _____                          _        ");
                Console.WriteLine(@"         / ____|                        | |       ");
                Console.WriteLine(@"        | |  __  __ _  __ _ _ __   ___  | |       ");
                Console.WriteLine(@"        | | |_ |/ _` |/ _` | '_ \ / _ \ | |       ");
                Console.WriteLine(@"        | |__| | (_| | (_| | | | |  __/ |_|       ");
                Console.WriteLine(@"         \_____|\__,_|\__, |_| |_|\___| (_)       ");
                Console.WriteLine(@"                       __/ |                      ");
                Console.WriteLine(@"                      |___/                       ");
                Console.WriteLine(@"             .                                    ");
                Console.WriteLine($"Bravo! Vous avez trouvé en {tour - 1} coups!      ");
                Console.WriteLine("Appuyez sur n'importe quelle touche pour quitter...");
                Console.ReadKey();
                #endregion
            }
            else
            {
                #region Perdu
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.DarkRed;

                Console.WriteLine(@"         _____             _                     ");
                Console.WriteLine(@"         |  __ \           | |                   ");
                Console.WriteLine(@"         | |__) |__ _ __ __| |_   _              ");
                Console.WriteLine(@"         |  ___/ _ \ '__/ _` | | | |             ");
                Console.WriteLine(@"         | |  |  __/ | | (_| | |_| |_ _ _        ");
                Console.WriteLine(@"         |_|   \___|_|  \__,_|\__,_(_|_|_)       ");
                Console.WriteLine(@"                                                 ");
                Console.WriteLine(@" Vous ferez peut-être mieux la prochaine fois... ");
                Console.WriteLine(@"Appuyez sur n'importe quelle touche pour quitter!");

                Console.ReadKey();
                #endregion
            } 
            #endregion
        }
    }
}           
            
   



